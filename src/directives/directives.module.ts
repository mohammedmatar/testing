import { NgModule } from '@angular/core';
import { FabricDirective } from './fabric/fabric';
import { CommonModule } from "@angular/common";
import { FabricServiceProvider } from "../providers/fabric-service/fabric-service";


@NgModule({
  declarations: [ FabricDirective ],
  imports: [ CommonModule ],
  providers: [
    FabricServiceProvider
  ],
  exports: [ FabricDirective ]
})
export class DirectivesModule {
}
