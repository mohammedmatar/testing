import { Directive, ElementRef, Renderer2, ViewChild } from '@angular/core';

import 'fabric'
declare let fabric: any;
/**
 *
 */
@Directive({
	selector: '[appFabric]' // Attribute selector
})
export class FabricDirective  {
	private canvasElem: HTMLCanvasElement;
	private fabricObj: any;
	private shape: any;
	constructor( private el: ElementRef, private renderer: Renderer2) {
		this.canvasElem = el.nativeElement;
		
		this.fabricObj = new fabric.Canvas(this.canvasElem, {
			isDrawingMode: true,
			width: this.canvasElem.width,
			height: 480
		});
		this.fabricObj.BaseBrush = new fabric.SprayBrush;
		this.fabricObj.freeDrawingBrush.color = 'pink';
		this.fabricObj.freeDrawingBrush.width = 5;
		
		const img = fabric.Image.fromURL('assets/imgs/logo.png', (img)=>{this.fabricObj.add(img);
		
		});
	}
	
}
