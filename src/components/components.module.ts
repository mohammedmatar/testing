import { NgModule } from '@angular/core';
import { DirectivesModule } from "../directives/directives.module";
import { DrawpadComponent } from './drawpad/drawpad';
import { IonicModule } from 'ionic-angular';

@NgModule({
	declarations: [ DrawpadComponent ],
	imports: [
		IonicModule,
		DirectivesModule
	],
	providers: [],
	exports: [ DrawpadComponent ]
})
export class ComponentsModule {
}
